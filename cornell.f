c	Proyecto Resolver potencial cornel
		
c para cambiar de linea, 5 espacios y &

c	las matrices las definimos con 'Dimension' y también las incluimos en doble precisión para tener más cifras significativas.
	double PRECISION r1, mb, sigma, rnmax, alfa, hc, pi,x,GA,
     & Norm,S,etan,rn, GA2,GA3,T,V,H,E,c,c2,ALPHAR, ALPHAI,S1,
     &BETA, VL, LDVL, VR, LDVR, WORK, LWORK,E2, r,phi1,phi2,suma,suma2,
     &a,b,cc,masa,Normalizacion,sumatorio
	integer n, nmax, l,m,j,INFO,nx,nr
	Dimension rn(40), etan(40), Norm(40), S(40,40),WORK(10000),
     &T(40,40), V(40,40),H(40,40),E(40),c(40,40),nx(3),
     &VL(40,40),LDVL(40),BETA(40),ALPHAR(40),ALPHAI(40),E2(3),
     &Normalizacion(3),phi1(40),S1(40,40)



	

	Parameter(r1=0.05)
	Parameter(mb=4.733)
	Parameter(sigma=0.207)
	Parameter(nmax=40)
	Parameter(rnmax=8.0)
	Parameter(alfa=0.366)
	Parameter(hc=0.197327)
	Parameter(pi=3.141592)

c abrimos un fichero para guardar los puntos
	open(10,FILE='WaveF.dat')	



c un bucle para los dos valores de l	
	do l=0,1


c definimos unos valores arbitrariamente altos para que entren en el bucle if. Las energías son de ordenes más bajos.
	E2(1)=10000.d0
	E2(2)=10000.d0
	E2(3)=10000.d0
	phi2=0.d0

c damos valores a los parametros del potencial cornel.
	do n=1,nmax		
	rn(n)=r1*(rnmax/r1)**((n-1.d0)/(nmax-1.d0))
	etan(n)=1.d0/(rn(n)**2.d0)
	end do

C---------------------------------------------------------------------------	
c bucle para la constate de normalización:
	
	do n=1,nmax
	
	Norm(n)=((2**(l+2)*(2.d0*etan(n))**(l+3.d0/2.d0))
     &/(pi**(5.d-1)*(2.d0*l+1.d0)))**5.d-1
	
c	write(*,*) Norm(n)
	end do


	
c------------------------------------------------------------------------------------------------
c 	CALCULAMOS LA MATRIZ S
c definimos una matriz igual, S1, pues la rutina dggev sobreescribe esta matriz y la necesitaremos luego
c para la normalizacion de los autovalores
	do n=1,nmax
	do j=1, nmax
	call Integral((2.d0*l+3.d0)/2.d0,(etan(n)+etan(j)),GA)
	S(n,j)=Norm(n)*Norm(j)*GA
    	S1(n,j)=S(n,j)
	
	end do
	end do
	
c----------------------------------------------------------------------------
c 	CALCULAMOS T y V
	do n=1,nmax
	do j=1, nmax

c hemos modificado el programa para realizar la integral:

c	En la primera entrada el ARGUMENTO de la función gamma (no confundir con la alfa de la integral), ARGUMENTO=(ALFA+3)/2
c	En la segunda entrada la betta de la integral
c	La tercera entrada nos devuelve el resultado de la integral.

	call Integral((2.d0*l+3.d0)/2.d0,(etan(n)+etan(j)),GA)
	call Integral((2.d0*l+5.d0)/2.d0,etan(n)+etan(j),GA2)

	T(n,j)=(-(hc**2)/mb)*Norm(n)*Norm(j)*(4.d0*etan(n)*etan(n)
     &*GA2-2.d0*etan(n)*(2.d0*l+3.d0)*GA)

	end do
	end do

c	hacemos los dos bucles 'do' de nuevo, para obtener los nuevos valores de las integrales para estas alfas y bettas.
	do n=1,nmax
	do j=1,nmax
	Call Integral(l+1.d0,etan(n)+etan(j),GA)
	Call Integral(l+2.d0,etan(n)+etan(j),GA2)
	Call Integral((2.d0*l+3.d0)/(2.d0),etan(n)+etan(j),GA3)
	V(n,j)=Norm(n)*Norm(j)*((-4.d0/3.d0)*(alfa*hc*GA)
     &+(sigma/(hc))*GA2+2.d0*mb*GA3)

	end do
	end do

c	do n=1,40
c	do j=1,nmax
c	write(*,*) n,j,GA,GA2,T(n,j), V(n,j)
c	end do
c	end do
	


c----------------------------------------------------------------------------
c	RESOLVER EL PROBLEMA DE AUTOVALORES

	do n=1,nmax
	do j=1,nmax
	H(n,j)=T(n,j)+V(n,j)
	end do
	end do

c	do n=1,nmax
c	write(*,*) (H(n,j), j=1,nmax)
c	end do


c llamamos a la rutina dggev, nos da los autovaloes y autoenergías H c=E S c


	call dggev( 'N', 'V', 40, H, 40, S, 40, ALPHAR, ALPHAI,
     & BETA, VL, 40, c, 40, WORK, 10000, INFO )
c----------------------------------------------------------
C		LA FUNCION DGGEV
c	dggev(JOBVL, JOBVR,N,A,LDA,B,LDB,ALPHAR,ALPHAI,
C		BETA, VL, LDVL, VR, LDVR, WORK, LWORK, INFO)

c WORK: vector de dimnesion igual al valor de LWORK, en nuestro caso igual a =10000
c LWORK, entero de entrada, mayor a 8*N, numero grande para trabajar.

C  DERECHA:  A*v=LAMBDA*B*v
C   IZQUIERDA u*H*A=LAMBDA*u*H*B

c	Resolvemos por la derecha: Hc=ESc--->E autovalores, c autovectores buscados.

c JOBVL: N para no hacer izquierda, V para hacer izquierda
c JOBVR: N para no hacer derecha, V para hacer derecha
c N: orden de las matrices  A,B,VL, VR
c A : matriz A de la ecuacion, será reescrita a la salida
c LDA: dimension de A
c B: Matriz B, reescrita a la salida tambien
c LDB dimension B

c autovales=(alphaR(j)+i*ALPHAI(j))/beta(j)

c VL: autovectores izquiera (u) por columnas, no estan ordenados de menos a mayor.
c 	si los autovaloes son complejos: parejas de complejos conjugados:
c					v(j)=VR(:,j)+i*VR(:,j+1) y v(j+1)=VR(:,j)-i*VR(:,j+1)

c LDVL: meter dimenion de VL
c VR
c LDVR igual para derecha

 
c INFO=0 esta bien

c-----------------------------------------------------------------------------------------	
c calculo autovalores, según el manual, los autovales son: 

	do n=1,nmax
	E(n)=ALPHAR(n)/BETA(n)
c	porque son reales ALPHAI=0
	end do
	

c	do n=1,nmax
c	write(*,*) 'Autovalores', n,E(n)
c	end do

c-----------------------------------------------------------------------

c	CALCULAMOS LOS 3 VALOES DE ENERGIA MÁS BAJOS

c 	los numeros nx(i) son contadores de posición para asociar el autovector con autovalor,
c	los autovectores por columnas en la posición del autovalor.

	do n=1,nmax
	if (E(n)<E2(1)) then
	E2(1)=E(n)
	nx(1)=n
	end if
	end do
	
	do n=1,nmax
	if (E(n)<E2(2) .AND. E(n)>E2(1)) then
	E2(2)=E(n)
	nx(2)=n
	end if
	end do
	
	do n=1,nmax
	if (E(n)<E2(3) .AND. E(n)>E2(2)) then
	E2(3)=E(n)
	nx(3)=n
	end if
	end do

c---------------------------------------------------------
c	Mostramos en pantalla las 3 Energías más bajas	
	do i=1,3
	write(*,'(A10,I4,F15.10)') 'Energía',i,E2(i)
	end do
c	A es para formato cadena de caracteres, I es para enteros y F es para reales.

c---------------------------------------------
c	NORMALIZACIÓN DE LOS AUTOVECTORES
	do nr=1,3
	suma=0.d0
	suma2=0.d0
	
	do n=1,nmax
	do j=1,nmax
	
	suma2=c(n,nx(nr))*S1(n,j)*c(j,nx(nr))
	suma=suma+suma2
	
	end do
	end do
	Normalizacion(nr)=suma
	end do


c------------------------------------------------------
c	FICHERO PARA FUNCION DE ONDAS RADIALES

c limpiamos las variables	
	do n=1,nmax
	phi1(n)=0.d0
	end do

	do nr=1,3	
	
	r=0.d0
	do j=0,99
C 	introducimos el valor del paso.
	r=0.d0+j*(1.5/99.d0)

	
	phi2=0.d0	
	do n=1,nmax
c los autovectores son c(:,nx(i)). Están por columnas, hay que recorrer todas las filas y
c el valor de la columna asociado a cada autovalor esta guardado en el contador nx(i)
	phi1(n)=Norm(n)*(r**(l))*exp(-etan(n)*r*r)*c(n,nx(nr))
	

	phi2=phi2+phi1(n)
	end do
c introducimos el valor de la normalizacion a la función de onda.
	phi2=phi2/sqrt(Normalizacion(nr))

c escribimos los valores en el fichero. I es para enteros y E para decimales.
	write(10,'(2I4,2E24.12)') nr,l,r,phi2
	
	end do
	end do


c----------------------------------------------------
c	VARIACIÓN DE MASA Y VARIABLE INFO.

c solo hacemos la variación de masa para l=0 y para nr=1, nivel fundamental.
	if (l==0) then
	nr=1	
	sumatorio=0.d0
	masa=0.d0
	do n=1,nmax
	do j=1,nmax
	sumatorio=Norm(n)*Norm(j)*c(n,nx(nr))*c(j,nx(nr))
     &/(Normalizacion(nr))
	masa=masa+sumatorio
	end do
	end do
	masa=masa*8.d0*alfa*((hc)**3)/(9.d0*mb*mb)

	write(*,*) 'Variación de masa(GeV)=',masa
	write(*,*) 'Variable INFO para l=0:',INFO
	else
	write(*,*) 'Variable INFO para l=1:',INFO
	end if
c-----------------------------------------------------
c terminamos el bucle do l=0,1 :
	end do

c cerramos el archivo de datos.
	close (10)
c----------------------------------------------
c	COMPARANDO MASAS

c	masa experimental=61.3 MeV
c	masa obtenida=131.5 MeV

c	No son iguales, errores en el modelo.

c--------------------------------------------	

C PRUEBA PARA GIT


	stop
	
	end








